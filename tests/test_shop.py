import unittest.mock as mock
from barbershop.config import read_config

@mock.patch('barbershop.config.open')
def test_read_config(mocked_open):
    read_config("foo.txt")
    mocked_open.assert_called_with('foo.txt', 'r')

