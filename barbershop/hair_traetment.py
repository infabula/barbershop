from .client import Client
from .barber import Barber


class HairTreatment:
    def __init__(self, barber=None, client=None):  # dependency injection
        self._barber = barber
        self._client = client

    def execute(self):
        print("A hair treatment done by ", self._barber._name, 'on client', self._client._name)