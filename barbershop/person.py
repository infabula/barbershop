class Person:
    '''Person class.'''

    def __init__(self, name, age, *argv, **kwargs):  # constructor
        '''Initializer.'''
        # properties
        self._name = name
        self._age = age
        self._profession = kwargs.get('profession', 'unknown')

    def set_age(self, age):
        print("set_age")
        if age < 0:
            raise ValueError("Age can't be negative")
        self._age = age
        return self  # chaining mogelijkheid

    def get_age(self):
        print("get_age")
        return self._age

    age = property(get_age, set_age)

    def __repr__(self):
        return self._name + ', ' + str(self.age)

    def __gt__(self, other):
        return self.age > other.age

    def __bool__(self):
        if self.age == 0:
            return False
        return True

    def greet(self):
        return "Hi, I am " + self._name