from barbershop.person import Person

class Barber(Person):
    def greet(self, test=False):
        if test:
            print("even voor de test")
        else:
            return "Hi I am your barber " + self._name

    def cut(self, client):
        print("Cutting the client")


if __name__ == "__main__":
    tony = Barber(name="Tony", age=34, profession='barber')
    tony.greet()