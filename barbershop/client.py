from .person import Person

class Client(Person):
    '''
        - haartype
        - haarlengte
        - haarkleur
        - gender
        - klanttype  
        '''
    # class data
    # hairtypes
    hairtypes = ('CURL', 'STRAIGHT', 'OTHER')

    @classmethod
    def print_hairtypes(cls):
        for hairtype in cls.hairtypes:
            print("-", hairtype)

    def __init__(self, name, age, *args, **kwargs):
        super().__init__(name, age, *args, **kwargs)
        self._hairtype = kwargs.get('hairtype', None)
        self._haircolor = kwargs.get('haircolor', None)
        self._gender = kwargs.get('gender', None)
        self._personality = kwargs.get('personality', None)

    @property
    def hairtype(self):
        return self._hairtype

    @hairtype.setter
    def hairtype(self, hairtype='OTHER'):
        if hairtype in Client.hairtypes:
            self._hairtype = hairtype

    def greet(self):
        message = "Hello, it's " + self._name
        if not self._haircolor is None:
            message = "Please cut my " + self._haircolor + " hair."

        return message
