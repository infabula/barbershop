import json
from flask import Flask
from flask import render_template
from flask_restful import Resource, Api

import requests

app = Flask(__name__)
api = Api(app)


@app.route('/hello')
def hello_world():
    return 'Hello, World!'

@app.route('/shop')
def shop():
    data = {'name': 'Cutting edge',
            'date': '1 maart 2021',
            'clients': ["Eve", "Bob", "Ella"]
            }
    return render_template('shop_opening.html', **data)

@app.route('/laadvermogen/<nummerplaat>')
def show_catalog_price(nummerplaat):
    url = "https://opendata.rdw.nl/resource/m9d7-ebf2.json?kenteken=" + nummerplaat.upper()
    print(url)
    api_session = requests.session()
    response = api_session.get(url)
    if response.status_code == 200:  # successfull response
        print(response.text)
        data = json.loads(response.text)  # convert to python list
        if data:
            weight = data[0].get('laadvermogen', 0)
            return "Het laadvermogen is" +  str(weight)
        else:
            return "Geen auto gevonden op die nummerplaat."
    return "Er gaat iets mis in de verbinding"

#------------------------------

class ClientResource(Resource):
    def get(self):
        return {'clients': ['Ella', 'Bob'] }
'''
    def post(self, data):
        # data in database opslaan

    def put(self, data):
        pass

    def delete(self):
        pass # delete het record
'''
api.add_resource(ClientResource, '/clients')