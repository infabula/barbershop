
from flask import Flask
import sys
import barbershop as bs
#from barbershop import Register
import shop_util



def main():
    '''Entry point of the program'''
    print("Running the barbershop")
    kassa = bs.Register(450)

    #for hairtype in bs.Client.hairtypes:
    #    print("-", hairtype)
    bs.Client.print_hairtypes()

    tony = bs.Barber(name="Tony", age=34, profession='barber')
    tony.profession = 'barber'

    print(tony.set_age(30).greet())


    ella = bs.Client(name="Ella", age=22, haircolor="brown")
    print(ella.greet())

    treatment = bs.HairTreatment(client=ella, barber=tony)
    treatment.execute()

if __name__ == "__main__":
    main()
    #run_webserver()